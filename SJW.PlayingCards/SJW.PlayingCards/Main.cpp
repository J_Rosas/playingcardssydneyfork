// Sydney Wayne
// 2/2/2021
// Playing Cards

#include <iostream>
#include <conio.h>

using namespace std;

enum Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

enum Suit
{
	Hearts,
	Diamonds,
	Spades,
	Clubs
};

struct Card
{
	Rank rank;
	Suit suit;
};

int main()
{


	(void)_getch();
	return 0;
}
